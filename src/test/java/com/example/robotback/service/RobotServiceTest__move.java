package com.example.robotback.service;

import com.example.robotback.model.MoveRecord;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class RobotServiceTest__move {
    private final RobotService robotService;

    @Autowired
    public RobotServiceTest__move(RobotService robotService) {
        this.robotService = robotService;
    }

    @Test
    public void when_moveCommand_is__f_plus_f__east__success() {
        String moveCommand = "f,f";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        int expectedX = moveRecord.getFromX() + 1;
        assertEquals(expectedX, moveRecord.getToX());
        System.out.println(moveRecord);
    }
    @Test
    public void when_moveCommand_is__r_plus_r__east__success() {
        String moveCommand = "r,r";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        int expectedY = moveRecord.getFromY() + 1;
        assertEquals(expectedY, moveRecord.getToY());
        System.out.println(moveRecord);
    }
    @Test
    public void when_moveCommand_is__l_plus_l__east__fail() {
        String moveCommand = "l,l";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        System.out.println(moveRecord);
    }

    @Test
    public void when_moveCommand_is__null() {
        String moveCommand = null;
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("-,-", moveRecord.getMoveCommand());
    }
    @Test
    public void when_moveCommand_is__empty() {
        String moveCommand = "";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("-,-", moveRecord.getMoveCommand());
    }
    @Test
    public void when_moveCommand_is__empty_plus_empty() {
        String moveCommand = ",";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("-,-", moveRecord.getMoveCommand());
    }
    @Test
    public void when_moveCommand_is__string_plus_empty() {
        String moveCommand = "abc,";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("ABC,-", moveRecord.getMoveCommand());
    }
    @Test
    public void when_moveCommand_is__empty_plus_string() {
        String moveCommand = ",abc";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("-,ABC", moveRecord.getMoveCommand());
    }
    @Test
    public void when_moveCommand_is__string_plus_string() {
        String moveCommand = "abc,def";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("ABC,DEF", moveRecord.getMoveCommand());
    }
    @Test
    public void when_moveCommand_is__f_plus_f() {
        String moveCommand = "f,f";
        MoveRecord moveRecord = robotService.move("simulationId", moveCommand);
        assertEquals("F,F", moveRecord.getMoveCommand());
        System.out.println(moveRecord);
    }
}
