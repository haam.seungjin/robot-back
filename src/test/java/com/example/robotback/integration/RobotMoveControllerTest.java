package com.example.robotback.integration;

import com.example.robotback.model.Constants;
import com.example.robotback.model.MoveRecord;
import com.example.robotback.model.MoveRequestBody;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RobotMoveControllerTest {
    private String getMoveRequestBody(String simulationId, String moveCommand)
            throws JsonProcessingException {
        MoveRequestBody moveRequestBody = new MoveRequestBody();
        moveRequestBody.setSimulationId(simulationId);
        moveRequestBody.setMoveCommand(moveCommand);
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(moveRequestBody);
    }

    private HttpHeaders getHttpHeaders(String auth) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        headers.add("Authorization", "Bearer your-access-token");
        headers.add("X-Custom-Header", "custom-value");
        return headers;
    }

    private MoveRecord integrationMoveTest(String simulationId, String moveCommand)
            throws JsonProcessingException {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "http://localhost:8080/v1/move";

        HttpHeaders httpHeaders = getHttpHeaders("");

        // make request
        String moveRequestBody = getMoveRequestBody(simulationId, moveCommand);
        RequestEntity<String> requestEntity = RequestEntity
                .method(HttpMethod.POST, URI.create(apiUrl))
                .headers(httpHeaders)
                .body(moveRequestBody);

        // invoke api
        ResponseEntity<String> response =
                restTemplate.exchange(apiUrl, HttpMethod.POST, requestEntity, String.class);

        // read response
        String responseBody = response.getBody();
        ObjectMapper objectMapper = new ObjectMapper();
        MoveRecord moveRecord = objectMapper.readValue(responseBody, MoveRecord.class);

        return moveRecord;
    }

    @Test
    public void test_figure8() throws JsonProcessingException {
        String simulationId = UUID.randomUUID().toString();
        MoveRecord moveRecord;
        // Start with x=0, y=0, d=E

        moveRecord = integrationMoveTest(simulationId, "F,F");
        // System.out.println("response: " + moveRecord);
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(1, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.E, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(2, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.E, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(3, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.E, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.E, moveRecord.getToD());

        // Collision
        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.ERROR, moveRecord.getCode());
        assertEquals(Constants.OUT_OF_AREA_ERROR, moveRecord.getErrorMessage());
        assertEquals(4, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.E, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,B");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.S, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(1, moveRecord.getToY());
        assertEquals(Constants.Direction.S, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(2, moveRecord.getToY());
        assertEquals(Constants.Direction.S, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(3, moveRecord.getToY());
        assertEquals(Constants.Direction.S, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.S, moveRecord.getToD());

        // Collision
        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.ERROR, moveRecord.getCode());
        assertEquals(Constants.OUT_OF_AREA_ERROR, moveRecord.getErrorMessage());
        assertEquals(4, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.S, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,B");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(4, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.W, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(3, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.W, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(2, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.W, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(1, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.W, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(0, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.W, moveRecord.getToD());

        // Collision
        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.ERROR, moveRecord.getCode());
        assertEquals(Constants.OUT_OF_AREA_ERROR, moveRecord.getErrorMessage());
        assertEquals(0, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.W, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,B");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(0, moveRecord.getToX());
        assertEquals(4, moveRecord.getToY());
        assertEquals(Constants.Direction.N, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(0, moveRecord.getToX());
        assertEquals(3, moveRecord.getToY());
        assertEquals(Constants.Direction.N, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(0, moveRecord.getToX());
        assertEquals(2, moveRecord.getToY());
        assertEquals(Constants.Direction.N, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(0, moveRecord.getToX());
        assertEquals(1, moveRecord.getToY());
        assertEquals(Constants.Direction.N, moveRecord.getToD());

        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.SUCCESS, moveRecord.getCode());
        assertEquals(0, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.N, moveRecord.getToD());

        // Collision
        moveRecord = integrationMoveTest(simulationId, "F,F");
        assertEquals(Constants.Code.ERROR, moveRecord.getCode());
        assertEquals(Constants.OUT_OF_AREA_ERROR, moveRecord.getErrorMessage());
        assertEquals(0, moveRecord.getToX());
        assertEquals(0, moveRecord.getToY());
        assertEquals(Constants.Direction.N, moveRecord.getToD());

    }
}
