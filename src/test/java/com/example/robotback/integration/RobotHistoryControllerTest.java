package com.example.robotback.integration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.client.RestTemplate;

public class RobotHistoryControllerTest {
    @Test
    public void test() {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "http://localhost:8080/v1/history?simulationId=513ec6f3-a8ef-4b6b-bf49-34caad9d17aa";

        // Make a GET request to the API endpoint
        ResponseEntity<String> response =
                restTemplate.exchange(apiUrl, HttpMethod.GET, null, String.class);

        // Retrieve the response body
        String responseBody = response.getBody();

        // Print the response body
        System.out.println("Response: " + responseBody);
    }

    @Test
    public void test2() {
        RestTemplate restTemplate = new RestTemplate();
        String apiUrl = "http://localhost:8080/v1/history?simulationId=513ec6f3-a8ef-4b6b-bf49-34caad9d17aa";

//        Assertions.assertThrows(HttpRequestMethodNotSupportedException.class, () -> {
//            ResponseEntity<String> response =
//                    restTemplate.exchange(apiUrl, HttpMethod.POST, null, String.class);
//        });

//        // Make a GET request to the API endpoint
//        try {
//            ResponseEntity<String> response =
//                    restTemplate.exchange(apiUrl, HttpMethod.POST, null, String.class);
//            System.out.println("----------response: " + response);
//
//            // Retrieve the response body
//            String responseBody = response.getBody();
//
//            // Print the response body
//            System.out.println("Response: " + responseBody);
//        } catch (Exception e) {
//            System.out.println("-----------------" + e);
//        }
    }
}
