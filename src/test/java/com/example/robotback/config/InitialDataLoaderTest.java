package com.example.robotback.config;

import com.example.robotback.model.Member;
import com.example.robotback.repository.MemberRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class InitialDataLoaderTest {
    private final InitialDataLoader initialDataLoader;
    private final MemberRepository memberRepository;

    @Autowired
    public InitialDataLoaderTest(
            InitialDataLoader initialDataLoader,
            MemberRepository memberRepository) {
        this.initialDataLoader = initialDataLoader;
        this.memberRepository = memberRepository;
    }

    @Test
    public void When_InitialDataLoader_is_initialized_10_initial_members_are_registered() {
        initialDataLoader.insertInitialData();
        List<Member> allMember = memberRepository.findAll();
        Assertions.assertEquals(10, allMember.size());
    }
}
