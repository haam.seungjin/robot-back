package com.example.robotback.repository;

import com.example.robotback.model.Constants;
import com.example.robotback.model.MoveRecord;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@SpringBootTest
public class MoveRecordRepositoryTest {
    private final MoveRecordRepository moveRecordRepository;

    @Autowired
    public MoveRecordRepositoryTest(MoveRecordRepository moveRecordRepository) {
        this.moveRecordRepository = moveRecordRepository;
    }

    @BeforeEach
    @AfterEach
    @Transactional
    void cleanup_db() {
        moveRecordRepository.deleteAll();
    }


    @Test
    public void The_object_not_stored_in_the_db_should_not_be_found_in_the_db() {
        Long id = 1L;

        Optional<MoveRecord> result = moveRecordRepository.findById(id);
        Assertions.assertFalse(result.isPresent(), "Member not found");
    }

    @Test
    public void The_object_read_from_the_DB_must_be_the_same_as_the_original_object() {
        String simulationId = UUID.randomUUID().toString();
        MoveRecord moveRecord = MoveRecord.builder()
                .id(1L)
                .simulationId(simulationId)
                .moveCommand("F,L")
                .fromX(1)
                .fromY(1)
                .fromD(Constants.Direction.N)
                .toX(0)
                .toY(1)
                .toD(Constants.Direction.S)
                .code(Constants.Code.SUCCESS)
                .errorMessage("")
                .timestamp(Instant.now().toString())
                .build();

        moveRecordRepository.save(moveRecord);
        List<MoveRecord> all = moveRecordRepository.findAll();
        Assertions.assertEquals(1, all.size());
        all.get(0).setId(1L);
        Assertions.assertEquals(moveRecord, all.get(0));
    }

    @Test
    public void If_save_multiple_times_there_should_be_same_number_of_record_in_the_db() {
        String simulationId = UUID.randomUUID().toString();
        MoveRecord moveRecord = MoveRecord.builder()
                .id(null)
                .simulationId(simulationId)
                .moveCommand("F,L")
                .fromX(1)
                .fromY(1)
                .fromD(Constants.Direction.N)
                .toX(0)
                .toY(1)
                .toD(Constants.Direction.S)
                .code(Constants.Code.SUCCESS)
                .errorMessage("")
                .timestamp(Instant.now().toString())
                .build();

        int maxLoop1 = 5;
        for(int i = 0; i < maxLoop1; ++i) {
            moveRecord.setId(null);
            moveRecordRepository.save(moveRecord);
        }

        int maxLoop2 = 50;
        for(int i = 0; i < maxLoop2; ++i) {
            moveRecord.setId(null);
            moveRecord.setSimulationId(UUID.randomUUID().toString());
            moveRecordRepository.save(moveRecord);
        }

        List<MoveRecord> all = moveRecordRepository.findAll();
        System.out.println("all size: " + all.size());

        List<MoveRecord> records = moveRecordRepository.findAllBySimulationIdOrderByIdAsc(simulationId);
        System.out.println("size: " + records.size());
        Assertions.assertEquals(records.size(), maxLoop1);
    }
}
