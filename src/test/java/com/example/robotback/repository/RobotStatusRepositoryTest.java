package com.example.robotback.repository;

import com.example.robotback.model.Constants;
import com.example.robotback.model.RobotStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@SpringBootTest
public class RobotStatusRepositoryTest {
    private final RobotStatusRepository robotStatusRepository;

    @Autowired
    public RobotStatusRepositoryTest(RobotStatusRepository robotStatusRepository) {
        this.robotStatusRepository = robotStatusRepository;
    }

    @AfterEach
    @Transactional
    void cleanup_db() {
        robotStatusRepository.deleteAll();
    }

    @Test
    public void The_object_not_stored_in_the_db_should_not_be_found_in_the_db() {
        String simulationId = UUID.randomUUID().toString();

        Optional<RobotStatus> result = robotStatusRepository.findById(simulationId);
        Assertions.assertFalse(result.isPresent(), "Member not found");
    }

    @Test
    public void The_object_read_from_the_DB_must_be_the_same_as_the_original_object() {
        String simulationId = UUID.randomUUID().toString();

        RobotStatus robotStatus = RobotStatus.builder()
                .simulationId(simulationId)
                .x(0)
                .y(0)
                .d(Constants.Direction.E)
                .build();

        robotStatusRepository.save(robotStatus);
        Optional<RobotStatus> result = robotStatusRepository.findById(simulationId);
        Assertions.assertTrue(result.isPresent(), "Member not found");

        RobotStatus actualValue = result.get();
        Assertions.assertEquals(robotStatus, actualValue);
    }

    @Test
    public void Even_if_save_multiple_times_there_should_be_only_one_record_in_the_db() {
        String simulationId = UUID.randomUUID().toString();

        RobotStatus robotStatus = RobotStatus.builder()
                .simulationId(simulationId)
                .x(0)
                .y(0)
                .d(Constants.Direction.E)
                .build();

        robotStatusRepository.save(robotStatus);
        robotStatusRepository.save(robotStatus);
        robotStatusRepository.save(robotStatus);

        List<String> ids = Arrays.asList(simulationId);;
        List<RobotStatus> allById = robotStatusRepository.findAllById(ids);
        Assertions.assertEquals(allById.size(), 1);
    }
}
