package com.example.robotback.repository;

import com.example.robotback.model.Member;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@SpringBootTest
public class MemberRepositoryTest {
    private final MemberRepository memberRepository;

    @Autowired
    public MemberRepositoryTest(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }

    @AfterEach
    @Transactional
    void cleanup_db() {
        memberRepository.deleteAll();
    }

    @Test
    public void The_object_not_stored_in_the_db_should_not_be_found_in_the_db() {
        String email = "user@company.com";

        Optional<Member> result = memberRepository.findById(email);
        Assertions.assertFalse(result.isPresent(), "Member not found");
    }

    @Test
    public void The_object_read_from_the_DB_must_be_the_same_as_the_original_object() {
        String email = "user@company.com";

        Set<String> permissions = new HashSet<String>() {{
            add("read");
            add("write");
        }};

        Member member = Member.builder()
                .email(email)
                .password("1234")
                .permissions(permissions)
                .build();

        memberRepository.save(member);
        Optional<Member> result = memberRepository.findById(email);
        Assertions.assertTrue(result.isPresent(), "Member not found");

        Member actualValue = result.get();
        Assertions.assertEquals(member, actualValue);
    }

    @Test
    public void Even_if_save_multiple_times_there_should_be_only_one_record_in_the_db() {
        String email = "user@company.com";

        Set<String> permissions = new HashSet<String>() {{
            add("read");
            add("write");
        }};

        Member member = Member.builder()
                .email(email)
                .password("1234")
                .permissions(permissions)
                .build();

        memberRepository.save(member);
        memberRepository.save(member);
        memberRepository.save(member);

        List<String> ids = Arrays.asList(email);;
        List<Member> allById = memberRepository.findAllById(ids);
        Assertions.assertEquals(allById.size(), 1);
    }
}
