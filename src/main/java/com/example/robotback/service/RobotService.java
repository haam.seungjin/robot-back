package com.example.robotback.service;

import com.example.robotback.model.Constants;
import com.example.robotback.model.MoveRecord;
import com.example.robotback.model.RobotStatus;
import com.example.robotback.repository.MoveRecordRepository;
import com.example.robotback.repository.RobotStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.Optional;

@Service
public class RobotService {
    private final RobotStatusRepository robotStatusRepository;
    private final MoveRecordRepository moveRecordRepository;
    @Autowired
    public RobotService(
            RobotStatusRepository robotStatusRepository,
            MoveRecordRepository moveRecordRepository ) {
        this.robotStatusRepository = robotStatusRepository;
        this.moveRecordRepository = moveRecordRepository;
    }

    public MoveRecord move(String simulationId, String moveCommand) {
        // Normalize moveCommand
        String normalizedMoveCommand = RobotUtil.getNormalizedMoveCommand(moveCommand);

        // Read beforeRobotStatus from DB
        Optional<RobotStatus> robotStatusOptional = robotStatusRepository.findById(simulationId);
        RobotStatus defaultRobotStatus = RobotStatus.builder()
                .x(0)
                .y(0)
                .d(Constants.Direction.E)
                .build();
        RobotStatus beforeRobotStatus = robotStatusOptional.orElse(defaultRobotStatus);

        // Define return value
        MoveRecord moveRecord = MoveRecord.builder()
                .simulationId(simulationId)
                .moveCommand(normalizedMoveCommand)
                .fromY(beforeRobotStatus.getY())
                .fromX(beforeRobotStatus.getX())
                .fromD(beforeRobotStatus.getD())
                .timestamp(Instant.now().toString())
                .build();

        // Calculate afterRobotStatus
        StringBuilder errorMessageBuilder = new StringBuilder();
        RobotStatus afterRobotStatus = calculateAfterRobotStatus(beforeRobotStatus, normalizedMoveCommand, errorMessageBuilder);

        // Move Error
        if(afterRobotStatus == null) {
            moveRecord.setToX(beforeRobotStatus.getX());
            moveRecord.setToY(beforeRobotStatus.getY());
            moveRecord.setToD(beforeRobotStatus.getD());
            moveRecord.setCode(Constants.Code.ERROR);
            moveRecord.setErrorMessage(errorMessageBuilder.toString());
            // Save move Log
            moveRecordRepository.save(moveRecord);
            return moveRecord;
        }

        // Save afterRobotStatus to DB
        afterRobotStatus.setSimulationId(simulationId);
        robotStatusRepository.save(afterRobotStatus);

        // Move success
        moveRecord.setToX(afterRobotStatus.getX());
        moveRecord.setToY(afterRobotStatus.getY());
        moveRecord.setToD(afterRobotStatus.getD());
        moveRecord.setCode(Constants.Code.SUCCESS);
        moveRecord.setErrorMessage("");

        // Save move Log
        moveRecordRepository.save(moveRecord);
        return moveRecord;
    }

    private RobotStatus calculateAfterRobotStatus(
            RobotStatus beforeRobotStatus,
            String normalizedMoveCommand,
            StringBuilder errorMessageBuilder) {

        int tmpDx = 0, tmpDy = 0, tmpDd =0;
        switch(normalizedMoveCommand) {
            case "F,F": tmpDy += -1; break;
            case "B,B": tmpDy += 1; break;
            case "L,L": tmpDx += -1; break;
            case "R,R": tmpDx += 1; break;
            case "F,B": tmpDd += 90; break;
            case "B,F": tmpDd += -90; break;
            case "F,-": tmpDy += -1; tmpDx += 1; tmpDd += 90; break;
            case "-,F": tmpDy += -1; tmpDx += -1; tmpDd += -90; break;
            default:
                errorMessageBuilder.append(Constants.UNDEFINED_MOVE_COMMAND_ERROR);
                return null;
        }

        int dx = 0, dy = 0, d = 0;
        switch(beforeRobotStatus.getD()) {
            case N: dy = tmpDy; dx = tmpDx; d = tmpDd; break;
            case E: dy = tmpDx; dx = -tmpDy; d = 90 + tmpDd; break;
            case S: dy = -tmpDy; dx = -tmpDx; d = 180 + tmpDd; break;
            case W: dy = -tmpDx; dx = tmpDy; d = 270 + tmpDd; break;
        }

        int x = beforeRobotStatus.getX() + dx;
        int y = beforeRobotStatus.getY() + dy;

        RobotStatus afterRobotStatus = RobotStatus.builder()
                .x(x)
                .y(y)
                .d(RobotUtil.calculateD(d))
                .build();

        // Robot is out of area
        if(RobotUtil.isValidStatus(afterRobotStatus) == false) {
            errorMessageBuilder.append(Constants.OUT_OF_AREA_ERROR);
            return null;
        }

        return afterRobotStatus;
    }
}
