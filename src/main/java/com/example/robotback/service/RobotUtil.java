package com.example.robotback.service;

import com.example.robotback.model.Constants;
import com.example.robotback.model.Grid;
import com.example.robotback.model.RobotStatus;

public class RobotUtil {
    public static Constants.Direction calculateD(int d) {
        d = (360 + d) % 360;
        switch (d) {
            case 0: return Constants.Direction.N;
            case 90: return Constants.Direction.E;
            case 180: return Constants.Direction.S;
            case 270: return Constants.Direction.W;
        }
        return Constants.Direction.UNKNOWN;
    }

    public static boolean isValidStatus(RobotStatus robotStatus) {
        int x = robotStatus.getX();
        int y = robotStatus.getY();

        if(x < 0 || y < 0 || x >= Grid.maxX || y >= Grid.maxY) {
            return false;
        }
        return true;
    }

    public static String getNormalizedMoveCommand(String moveCommand) {
        if (moveCommand == null || moveCommand.isEmpty()) {
            return "-,-";
        }
        moveCommand = moveCommand.toUpperCase();
        String leftCommand = "";
        String rightCommand = "";
        boolean isLeftCommand = true;
        for(int i = 0; i < moveCommand.length(); ++i) {
            if(moveCommand.charAt(i) == ',') {
                isLeftCommand = false;
            }
            else if(moveCommand.charAt(i) != ' ') {
                if(isLeftCommand) {
                    leftCommand += moveCommand.charAt(i);
                }
                else {
                    rightCommand += moveCommand.charAt(i);
                }
            }
        }

        if(leftCommand.isEmpty()) leftCommand = "-";
        if(rightCommand.isEmpty()) rightCommand = "-";

        return leftCommand + ',' + rightCommand;
    }
}
