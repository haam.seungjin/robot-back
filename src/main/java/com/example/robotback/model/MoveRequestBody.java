package com.example.robotback.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class MoveRequestBody {
    @NotNull
    @NotBlank
    @Size(min = 1, max = 50)
    private String simulationId;

    @NotNull
    @NotBlank
    @Size(min = 1, max = 50)
    private String moveCommand;
}
