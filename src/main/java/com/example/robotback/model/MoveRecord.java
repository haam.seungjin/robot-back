package com.example.robotback.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "move_record")
public class MoveRecord {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String simulationId;
    private String moveCommand;
    private int fromX;
    private int fromY;
    private Constants.Direction fromD;
    private int toX;
    private int toY;
    private Constants.Direction toD;
    private Constants.Code code;
    private String errorMessage;
    private String timestamp;
}
