package com.example.robotback.model;

public class Constants {
    private Constants() {
        // private constructor to prevent instantiation
    }

    public enum Code {
        ERROR,
        SUCCESS,
    }

    public enum Direction {
        N,
        E,
        S,
        W,
        UNKNOWN,
    }

    public static final int MAX_VALUE = 100;
    public static final double PI = 3.14159;
    public static final String UNDEFINED_MOVE_COMMAND_ERROR = "[Error] Undefined movement";
    public static final String OUT_OF_AREA_ERROR
            = "[Error] The command was ignored because the robot cannot move outside the area.;";

    public static final RobotStatus defaultRobotStatus = RobotStatus.builder()
            .simulationId("")
            .x(0)
            .y(0)
            .d(Constants.Direction.E)
            .build();
}
