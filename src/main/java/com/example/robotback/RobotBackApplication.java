package com.example.robotback;

import com.example.robotback.model.Member;
import com.example.robotback.repository.MemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RobotBackApplication {
	public static void main(String[] args) {
		SpringApplication.run(RobotBackApplication.class, args);
	}
}
