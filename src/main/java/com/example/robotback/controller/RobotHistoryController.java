package com.example.robotback.controller;

import com.example.robotback.model.MoveRecord;
import com.example.robotback.repository.MoveRecordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@CrossOrigin(origins = "*", methods = RequestMethod.GET)
@RequestMapping("/v1")
public class RobotHistoryController {
    private final MoveRecordRepository moveRecordRepository;

    @Autowired
    public RobotHistoryController(MoveRecordRepository moveRecordRepository) {
        this.moveRecordRepository = moveRecordRepository;
    }

    @GetMapping("/history")
    @ResponseBody
    public ResponseEntity history(@RequestParam("simulationId") String simulationId) {
        if (simulationId == null || simulationId.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid simulationId");
        }

        List<MoveRecord> moveRecords = moveRecordRepository.findAllBySimulationIdOrderByIdAsc(simulationId);
        return ResponseEntity.ok(moveRecords);
    }
}
