package com.example.robotback.controller;

import com.example.robotback.model.MoveRecord;
import com.example.robotback.model.MoveRequestBody;
import com.example.robotback.service.RobotService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@CrossOrigin(origins = "*", methods = RequestMethod.POST)
@RequestMapping("/v1")
public class RobotMoveController {
    private final RobotService robotService;
    private final ObjectMapper objectMapper;

    @Autowired
    public RobotMoveController(RobotService robotService, ObjectMapper objectMapper) {
        this.robotService = robotService;
        this.objectMapper = objectMapper;
    }

    @PostMapping("/move")
    @ResponseBody
    @Validated
    public ResponseEntity move(@Valid @RequestBody MoveRequestBody moveRequestBody,
                           BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(bindingResult.getAllErrors());
        }

        String simulationId = moveRequestBody.getSimulationId();
        String moveCommand = moveRequestBody.getMoveCommand();
        if(simulationId == null || simulationId.isEmpty()
        || moveCommand == null || moveCommand.isEmpty()) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Invalid requestBody");
        }

        MoveRecord moveRecord = robotService.move(moveRequestBody.getSimulationId(), moveRequestBody.getMoveCommand());
        return ResponseEntity.ok(moveRecord);
    }
}
