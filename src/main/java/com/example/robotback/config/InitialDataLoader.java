package com.example.robotback.config;

import com.example.robotback.model.Member;
import com.example.robotback.repository.MemberRepository;
import jakarta.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class InitialDataLoader {
    private MemberRepository memberRepository;

    @Autowired
    public InitialDataLoader(MemberRepository memberRepository) {
        this.memberRepository = memberRepository;
    }
    @PostConstruct
    @Transactional
    public void insertInitialData() {
        for(int i = 0; i < 10; ++i) {
            Member member = Member.builder()
                    .email("guest" + i)
                    .password("1234")
                    .build();
            memberRepository.save(member);
        }
    }
}
