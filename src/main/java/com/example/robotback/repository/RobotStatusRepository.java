package com.example.robotback.repository;

import com.example.robotback.model.RobotStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RobotStatusRepository extends JpaRepository<RobotStatus, String> {
}
