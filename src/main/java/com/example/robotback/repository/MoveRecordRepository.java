package com.example.robotback.repository;

import com.example.robotback.model.MoveRecord;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MoveRecordRepository extends JpaRepository<MoveRecord, Long> {
        List<MoveRecord> findAllBySimulationIdOrderByIdAsc(String customColumn);
}
